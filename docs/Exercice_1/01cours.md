---
author: Jacques-Olivier BOUDIER
title: Exercice 1
tags:
  - brassage génétique
  - brassage interchromosomique
---
Voici le sujet du premier exercice.

![Sujet 1](Images/exercice1.png)


## I. Premières informations à rechercher

Avant d'interpréter le deuxième croisement, il me faut quelques informations. Quels sont les gènes impliqués ? Quels sont les allèles pour chaque gène ? Quelle est la dominance ou la récessivité des allèles ?

???+ question "Questions"

    **1.** Quels sont les gènes impliqués dans ce croisement ?

    ??? success "Solution"

        Comme dans tout exercie de dihybridisme, il y a deux gènes impliqués :

        - une gène codant pour la couleur du corps ;

        - un autre gène codant pour la longueur des ailes.


    **2.** Quels sont les allèles dominants ?

    ??? success "Solution"

        C'est la génération F1 qui va me fournir la réponse.
        La première loi de mendel stipule l'homogénéité de la génération F1. 
        Dans ce cas précis, les deux allèles dominants sont donc les allèles :

        - corps gris (G);
        
        - aile longue (L).
      
    **3.** Quels sont les allèles récessifs ?

    ??? success "Solution"

        C'est la génération F1 qui va me fournir la réponse.
        Dans ce cas précis, les deux allèles récessifs sont donc les allèles :

        - corps ébène (e);
        
        - aile vestigiale (vg).


## II. Intérêt du deuxième croisement

Le second croisement est un [croisement-test](../definitions.md). Son but est de déterminer le génotype des gamètes de la génération F1.


???+ question "Questions"

    **4.** Combien de phénotypes obtient-on avec ce croisement ? Dans quelles proportions ?

    ??? success "Réponse"

        Le résultat du croisement montre quatre phénotypes différents en proportions égales : les quatre phénotypes sont donc **équiprobables**.


    **5.** Quelles informations ce croisement apporte-t-il sur la localisation des gènes ?

    ??? success "Réponse"

        Le croisement-test implique un *individu homozygote double récessif* (ici P2). Cet individu n'apporte qu'un seul type de gamète.

        Comme nous obtenons 4 phénotypes différents à l'issue de ce croisement, cela veut donc dire que l'individu F1 est capable de produire 4 gamètes différents : les gènes sont donc [indépendants](../definitions.md), portés par deux paires de chromosomes différentes.

        Les résultats obtenus correspondent à une distribution équiprobable des chromosomes de part et d’autre de l’équateur de la cellule en méiose, c'est-à-dire à un simple **brassage [interchromosomique](../definitions.md)**.
        
        


## III. Vérification par un échiquier de croisement

Pour compléter la réponse aux questions précédentes, on peut démontrer la véracité de notre conclusion à l'aide d'un échiquier de croisement qui va illustrer le croisement-test.

??? tip "Exemple d'échiquier de croisement"

    Dans le tableau ci-dessous, ce qui est écrit en vert est déduit des informations du sujet.
    
    ![Echiquier de croisement](Images/EC_exo1.png)