---
author: Jacques-Olivier BOUDIER 
title:  Accueil
---

# Les brassages génétiques en terminale spé


L’*analyse génétique* peut se fonder sur l’étude de la transmission héréditaire des caractères observables (phénotype) dans des croisements issus le plus souvent de lignées pures (homozygotes) et ne différant que par un nombre limité de caractères.

Il s'agit donc le plus souvent de déduire de l'analyse de phénotypes obtenus lors de croisements les génotypes associés.


Les croisements expérimentaux proposés vont nous permettre d’expliquer les mécanismes à l’origine de l’apparition de phénotypes nouveaux reflétant de nouvelles combinaisons génétiques formées lors de la
reproduction sexuée.

L'un des modèles exprimentaux les plus utilisés en génétique est la Drosophile (*Drosophila melanogaster*).

Les Drosophiles sont de petites mouches qui se reproduisent rapidement en laboratoire. Elles possèdent de nombreux caractères morphologiques facilement observables, à l'état sauvage ou muté: longueur des ailes, couleur du corps, couleur des yeux...

![Drosophile](Images/Drosophila.jpg)

__source:__ https://fr.wikipedia.org/wiki/Drosophile

Ce site présente des exercices corrigés. 

**Pour profiter pleinement de ces pages, munissez vous d'une feuille et essayez de faire les exercices avant d'afficher la correction.**


