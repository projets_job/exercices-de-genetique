---
author: Jacques-Olivier BOUDIER
title: Exercice 2
tags:
  - brassage génétique
  - brassage intrachromosomique
  - crossing-over
---
Voici le sujet du deuxième exercice.

![Sujet 2](Images/exercice2.png)


## I. Premières informations à rechercher

Avant d'interpréter le deuxième croisement, il me faut quelques informations. Quels sont les gènes impliqués ? Quels sont les allèles pour chaque gène ? Quelle est la dominance ou la récessivité des allèles ?

???+ question "Questions"

    **1.** Quels sont les gènes impliqués dans ce croisement ?

    ??? success "Solution"

        Comme dans tout exercie de dihybridisme, il y a deux gènes impliqués :

        - une gène codant pour l'aspect de l'abdomen (rayé ou uni) ;

        - un autre gène codant pour la présence de soies (=poils).


    **2.** Quels sont les allèles dominants ?

    ??? success "Solution"

        C'est la génération F1 qui va me fournir la réponse.
        La première loi de mendel stipule l'homogénéité de la génération F1. 
        Dans ce cas précis, les deux allèles dominants sont donc les allèles :

        - abdomen uni (U)
        
        - thorax muni de soies (S+)
      

## II. Le deuxième croisement

Dans cet exercice, on réalise un second croisement appelé [croisement-test](../definitions.md) entre une femelle F1 et un mâle homozygote double récessif.

???+ question "Questions"

    **3.** Combien de phénotypes obtient-on avec ce croisement ? Dans quelles proportions ?

    ??? success "Solution"

        Le résultat du croisement montre quatre phénotypes différents en proportions inégales : deux phénotypes sont majoritaires (80% des individus) et deux phénotypes sont plus rares (20% des individus). Les proportions ne sont donc pas équiprobables (= différentes de 4 fois 25%).


    **4.** Quelles informations ce croisement apporte-t-il sur la localisation des gènes ?

    ??? success "Solution"

        Le croisement-test implique un *individu [homozygote](../definitions.md) double récessif* (ici le mâle double récessif). Cet individu n'apporte qu'un seul type de gamète. Comme nous obtenons 4 phénotypes différents à l'issue de ce croisement, cela veut donc dire que la femelle F1 est capable de produire 4 gamètes différents mais dans des proportions différentes : les gènes sont donc [liés](../definitions.md), portés par une seule paire de chromosomes.
       
        Normalement, on devrait dans ce cas n'avoir que deux types de gamètes puisque les gènes sont liés. Comme ce n'est pas le cas, on envisage un **[brassage intrachromosomique](../définitions.md)** qui implique un crossing-over.
    
## III. Validation par un échiquier de croisement

Pour compléter la réponse aux questions précédentes, on peut démontrer la véracité de notre conclusion à l'aide d'un échiquier de croisement qui va illustrer le croisement-test.

??? tip "Exemple d'échiquier de croisement"

    Dans le tableau ci-dessous, ce qui est écrit en vert est déduit des informations du sujet.
    
    ![Echiquier de croisement](Images/EC_Bintra.PNG)

Comment expliquer l'apparition de phénotypes recombinés ? Il faut chercher du côté de la fabrication des gamètes chez la femelle F1 ( donc dessiner une méiose).

??? tip "Crossing-over"
    
    ![Crossing-over](Images/explicat_CO.PNG)