---
author: Jacques-Olivier BOUDIER
title: Exercice 3
tags:
  - brassage génétique
  - brassage interchromosomique
  - brassage interchromosomique
  - Localisation des gènes
---

# La localisation des gènes


**Étude de la répartition des gènes chez diverses espèces**

À ce jour, la répartition des gènes a été largement élucidée pour de nombreuses espèces. Cette avancée dans la compréhension du génome résulte d'un ensemble de techniques et de tests spécifiques visant à localiser les gènes d'intérêt sur les différents chromosomes. De plus, ces approches ont permis de déterminer les relations de liaison génétique entre certains gènes, contribuant ainsi à une meilleure compréhension des interactions génétiques et des mécanismes héréditaires.

**Le but de cet exercice est d'établir la carte génétique simplifiée du génome de la drosophile.**


## Les croisements


??? example "Technique utilisée"
    La détermination de la position des gènes chez la drosophile (mouche du vinaigre) par des croisements repose sur l'observation de la transmission des caractères héréditaires à travers plusieurs générations. 
    
    Voici comment cela fonctionne, étape par étape :

    ### 1. **Sélection des gènes à étudier**
    On choisit d'abord deux ou plusieurs gènes d'intérêt, c'est-à-dire des gènes qui contrôlent des traits visibles (phénotypes) chez la drosophile. Par exemple, on peut étudier la couleur des yeux et la forme des ailes.

    ### 2. **Croisement initial**
    On réalise un croisement entre deux individus de phénotypes distincts (généralement un parent double homozygote pour les traits étudiés et un autre parent homozygote récessif pour ces mêmes traits). Cela permet d'obtenir une première génération (F1) homogène (hétérozygote pour les gènes d'intérêt).

    ### 3. **Croisement-test**
    Les individus de la génération F1 sont ensuite croisés avec des individus homozygotes récessifs pour les deux caractères étudiés (croisement test). Cela génère une génération F2 qui montre une recombinaison des caractères.

    ### 4. **Observation des phénotypes dans la F2**
    En analysant les phénotypes de la génération F2, on peut identifier les individus qui ont hérité des combinaisons de traits différents de ceux des parents de la F1. Ces phénotypes recombinants résultent de la recombinaison génétique entre les gènes étudiés lors de la méiose.

    ### 5. **Calcul des fréquences de recombinaison**
    Les fréquences des phénotypes recombinants par rapport aux phénotypes parentaux sont ensuite calculées. Les gènes qui sont plus proches les uns des autres sur le même chromosome ont une probabilité plus faible de se recombiner, alors que les gènes plus éloignés se recombinent plus fréquemment.

    ### 6. **Établissement de la distance génétique**
    La fréquence de recombinaison est utilisée pour calculer la distance génétique entre les gènes. Cette distance est exprimée en centimorgans (cM), où 1 % de recombinaison équivaut à 1 cM. Ainsi, si 10 % des descendants montrent une recombinaison, cela signifie que les gènes sont séparés par une distance de 10 cM.

    ### 7. **Création de cartes génétiques**
    En répétant ces expériences avec différents gènes, il est possible de construire une carte génétique du chromosome, qui indique la position relative des gènes. Ces cartes sont essentielles pour comprendre l'organisation génétique et les relations de liaison entre les gènes.
  
    
    Cette méthode de croisement génétique, développée notamment par **Thomas Hunt Morgan** et ses collègues, a permis de cartographier les gènes sur les chromosomes de la drosophile et de comprendre les principes de l'hérédité liée aux chromosomes.

**Voici quatre croisements réalisés chez la drosophile. A partir des résultats obtenus lors du test-cross, recherchez la position relative des gènes les uns par rapport aux autres.**


!!! example "Croisements"

    === "Croisement 1" 
        (allèles eb et vg)

        On croise un parent 1 de type sauvage (corps jaune et ailes longues) avec un parent 2 au corps noir et aux ailes vestigiales (=courtes).

        On obtient une F1 constituée à 100% d'individus au corps jaune et aux ailes longues.

        On réalise un test-cross entre un individu de la génération F1 et le parent P2. 
        
        On obtient alors :

        - 25% d'individus au corps jaune et aux ailes longues ;

        - 25% d'individus au corps noir et aux ailes vestigiales ;

        - 25% d'individus au corps jaune et aux ailes vestigiales ;

        - 25% d'individus noirs aux ailes longues.

        Qu'en concluez-vous ?

    === "Croisement 2" 
        (allèles eb et sc)

        On croise un parent 1 de type sauvage (corps jaune et yeux rouge) avec un parent 2 au corps noir et aux yeux rouge vif (allèle scarlet sc).

        On obtient une F1 constituée à 100% d'individus au corps jaune et aux yeux rouge.

        On réalise un test-cross entre un individu de la génération F1 et le parent P2. 
        
        On obtient alors :

        - 37% d'individus au corps jaune et aux yeux rouge ;

        - 37% d'individus au corps noir et aux yeux rouge vif ;

        - 13% d'individus au corps noir et aux yeux rouge ;

        - 13% d'individus au corps jaune et aux yeux rouge vif.

        Qu'en concluez-vous ?

    === "Croisement 3" 
        (allèles bw et vg)

        On croise un parent 1 de type sauvage (yeux rouge et ailes longues) avec un parent 2 aux yeux bruns  et aux ailes vestigiales (=courtes).

        On obtient une F1 constituée à 100% d'individus aux yeux rouge et aux ailes longues.

        On réalise un test-cross entre un individu de la génération F1 et le parent P2. 
        
        On obtient alors :

        - 31,5% d'individus aux yeux rouge et aux ailes longues ;

        - 31,5% d'individus aux yeux bruns et aux ailes vestigiales ;

        - 18,5% d'individus aux yeux bruns et aux ailes longues ;

        - 18,5% d'individus aux yeux rouge et aux ailes vestigiales.

        Qu'en concluez-vous ?

    === "Croisement 4" 
        (allèles cn et vg)
        
        On croise un parent 1 de type sauvage (yeux rouge et ailes longues) avec un parent 2 aux yeux rouge vif (allèle cn)  et aux ailes vestigiales (=courtes).

        On obtient une F1 constituée à 100% d'individus aux yeux rouge et aux ailes longues.

        On réalise un test-cross entre un individu de la génération F1 et le parent P2. 
        
        On obtient alors :

        - 45% d'individus aux yeux rouge et aux ailes longues ;

        - 45% d'individus aux yeux rouge vif et aux ailes vestigiales ;

        - 5% d'individus aux yeux rouge vif et aux ailes longues ;

        - 5% d'individus aux yeux rouge et aux ailes vestigiales.

        Qu'en concluez-vous ?

## Interpréter les résultats

Pour localiser deux gènes l'un par rapport à l'autre, il faut savoir si ces gènes sont [liés](../definitions.md) ou [indépendants](../definitions.md).

Nous disposons de résultats issus de croisements-test. Dans ce croisement, les différents phénotypes ne dépendent que des allèles apportés par l'individu hétérozygote de la génération F1. 

Les proportions obtenues vont permettre de connaître la localisation des deux gènes impliqués :

- si on obtient 4 phénotypes en proportions égales (25%), l'individu F1 est donc capable de produire 4 combinaisons alléliques, c'est donc qu'il y a eu un brassage interchromosomique : les deux gènes considérés sont **indépendants** c'est à dire *portés par deux chromosomes différents* ;

??? tip "Schéma de méiose avec brassage interchromosomique"

    ![méiose avec gènes indépendants](Images/genesindependants.png)

    source : https://svt.ac-versailles.fr/IMG/pdf/Schema_bilan_brassages.pdf


- si on obtient 2 phénotypes en proportions égales (50%), l'individu F1 produit donc deux combinaisons alléliques seulement, c'est donc que les gènes sont **situés sur le même chromosome** ;

- si on obtient 4 phénotypes en proportions différents (deux phénotypes parentaux majoritaires et deux phénotypes recombinés minoritaires), c'est donc que l'individu F1 peut produire 4 combinaisons alléliques , dont deux plus rares ce qui témoigne d'un brassage intrachromosomique : les gènes sont **liés** c'est à dire *portés par le même chromosome*.

??? tip "Schéma de méiose avec brassage intrachromosomique"

    ![méiose avec gènes liés](Images/geneslies.png)

    source : https://svt.ac-versailles.fr/IMG/pdf/Schema_bilan_brassages.pdf

??? success "Pour aller plus loin"
    --Notion hors programme --
    On peut situer les gènes portés par le même chromosome les uns par rapport aux autres en estimant la distance génétique qui les sépare : elle correspond à un segment de chromatide sur lequel la probabilité qu'un enjambement s'effectue est de 1 %. L'unité de mesure est le **cM**(**centiMorgan** en l'honneur du généticien Morgan).Le morgan est donc l'intervalle où se produit en moyenne un événement de recombinaison par enjambement lors de la méiose.
    La distance génétique augmente avec la taille de l'intervalle séparant deux marqueurs : lorsque la distance entre deux gènes situés sur le même chromosome grandit, la probabilité qu'il se produise un enjambement grandit également.
    Il existe de nombreuses méthodes de calcul pour estimer la distance génétique...


## Carte génétique de la drosophile

Vous allez pouvoir vérifier vos prédictions en consultant la carte génétique de la drosophile.

Dans cet exercice, vous avez utilisé les allèles suivants :

- couleur noire --> eb

- ailes vestigiales --> vg

- yeux rouge vif --> cn ou sc (deux gènes différents)

- yeux brun --> bw

??? note "Carte génétique de la drosophile"

    ![Carte génétique de la drosophile](Images/carte.png)