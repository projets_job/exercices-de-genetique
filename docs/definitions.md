---
author: Jacques-Olivier BOUDIER 
title:  Définitions
---

# Les brassages génétiques en terminale spé

La reproduction sexuée est source de diversité génétique. Cette diversité apparaît lors de la formation des gamètes ainsi qu'au moment de la fécondation.

Les notions ci-dessous sont à connaître...

??? note pliée "Brassage intrachromosomique"

    Réassociation, au cours de la prophase de la première division de méiose, de chromatides homologues qui se sont cassées, puis recollées (*crossing-over*) ce qui conduit à des chromosomes recombinés.

??? note pliée "Brassage interchromosomique"

    Répartition aléatoire, au cours de l'anaphase de la première division de méiose, des chromosomes dupliqués (chromosomes à deux chromatides) de chaque paire 

??? note pliée "Croisement-test"

    En anglais : *test-cross*

    Croisement d'un individu dont on veut connaître le génotype avec un homozygote récessif (porteur des allèles récessifs des gènes considérés).

??? note pliée "Gènes indépendants"

    L'indépendance des gènes constitue la deuxième loi de Mendel. Les gènes sont indépendants s'ils sont portés par des paires différentes de chromosomes.

    Dans le cas d'un croisement de 2 F1 (hétérozygote pour les 2 gènes), on obtient une F2 avec les proportions 9/3/3/1 et dans le cas d'un test-cross, chaque phénotype compte pour 25% du total.

??? note pliée "Gènes liés"

    Gènes qui ont tendance à être hérités ensemble, en opposition avec la deuxième loi de Mendel qui stipule l'assortiment indépendant de chaque gène.

??? note pliée "Homozygote"

    Individu qui possède pour un même gène le même allèle sur les deux chromosomes homologues (= de la même paire)

??? note pliée "Hétérozygote"

    Individu qui possède pour un même gène un allèle différent sur chacun des deux chromosomes homologues (= de la même paire)

