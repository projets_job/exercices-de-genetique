---
author: Jacques-Olivier BOUDIER
title: 👏 Crédits
---
_Sources_ :

* Dictionnaire des Sciences de la Vie et de la Terre (M.Breuil / ed. Nathan)
* [Drososimul](https://svt.ac-versailles.fr/IMG/DrosoSimul/data/index.html)

Le site est hébergé par <a href="https://docs.forge.apps.education.fr/">la forge des communs numériques éducatifs </a>

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/){target="_blank"} et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/){target="_blank"}, et surtout [Pyodide-Mkdocs-Theme](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/){target="_blank"} pour la partie Python nécessaire pour les QCM.

